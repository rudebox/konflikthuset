<?php

/*
 * Template Name: Contact template
 */

get_template_part('parts/header'); the_post(); 

$text = get_field('contact_info');
$form_id = get_field('contact_form_id');

?>

<main>

	<?php get_template_part('parts/page', 'header');?>

	<section class="contact padding--both">
		<div class="wrap hpad">
			<div class="row">

				<div class="col-sm-6 contact__info">
					<?php echo $text; ?>
				</div>

				<div class="col-sm-6">
					<?php 
						gravity_form( $form_id, $display_title = false, $display_description = false, $display_inactive = false, $field_values = null, $ajax = false, 2, $echo = true ); 
					?>
				</div> 

			</div>
		</div>
	</section>
</main>

<?php get_template_part('parts/footer'); ?>