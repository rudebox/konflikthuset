jQuery(document).ready(function($) {

  //menu toggle
  $('.nav-toggle').click(function(e) {
    e.preventDefault();
    $('.nav--mobile').toggleClass('is-open');
    $('body').toggleClass('is-fixed');
  });


  //owl slider/carousel
  var owl = $(".slider__track");

   owl.each(function() {
    $(this).children().length > 1;

    owl.owlCarousel({
        loop: false,
        items: 1,
        autoplay: true,
        // nav: true,
        autplaySpeed: 11000,
        autoplayTimeout: 10000,
        smartSpeed: 250,
        smartSpeed: 2200,
        navSpeed: 2200
        // navText : ["<i class='fa fa-arrow-left' aria-hidden='true'></i>", "<i class='fa fa-arrow-right' aria-hidden='true'></i>"]
    });
  });

  $(function() {
      pinIt();
  });

  function pinIt() {    
      $('.map__pin').each(function(i) {
      var pin = $(this);

        setTimeout(function(){

          pin.addClass('is-active');

            setTimeout(function() {

              pin.removeClass('is-active');

                if (i == $('.map__pin').length-1) pinIt();;

            }, 600);
        }, 600 * i);
      });
  }

  //toggle contact forms
  $('#js-btn-phone').on('click', function() {
    $(this).addClass('is-active');
    $('#js-form-phone').addClass('is-active');
    $('#js-form-message').removeClass('is-active');
    $('#js-btn-message').removeClass('is-active');
  });

  $('#js-btn-message').on('click', function() {
    $(this).addClass('is-active');
    $('#js-form-phone').removeClass('is-active');
    $('#js-form-message').addClass('is-active');
    $('#js-btn-phone').removeClass('is-active');
  });

  //rotate on scroll
  $(window).scroll(function() {
    var mask = $(window).scrollTop();
    var op = $(window).scrollTop();
    mask = mask * 0.2;
    $('.page__mask').css({ transform: 'rotate(' + mask + 'deg)' });
  });


  //increment by 1 and add to class
  $i = 0;

  $('.page__breadcrumbs span a').each(function() {
    $i++;
    $(this).addClass('page__breadcrumbs--links');
    $(this).addClass('page__breadcrumbs--links--' + $i);
  });

});
