<?php get_template_part('parts/header'); the_post(); ?>

<main>

  <?php get_template_part('parts/page', 'header'); ?>

  <section class="single padding--both">

    <div class="wrap hpad">

      <article class="single__item" itemscope itemtype="http://schema.org/BlogPosting">

        <div itemprop="articleBody">
          <?php the_content(); ?>
        </div>

      </article>

    </div>

  </section>

</main>

<?php get_template_part('parts/footer'); ?>