<?php get_template_part('parts/header'); ?>

<main>

<?php get_template_part('parts/page', 'header'); ?>

  <section class="home padding--both">

    <div class="wrap hpad">
      <div class="row">

        <?php if (have_posts()): ?>
          <?php while (have_posts()): the_post(); ?>

           <?php $thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'url' );?>

            <a href="<?php the_permalink(); ?>" class="home__item col-sm-8 col-sm-offset-2" itemscope itemtype="http://schema.org/BlogPosting">

              <div class="row flex flex--wrap flex--justify">
              
                <div class="col-sm-8">
                  <header>
                    <h2 class="home__title" itemprop="headline" title="<?php the_title_attribute(); ?>">
                        <?php the_title(); ?>
                    </h2>
                  </header>

                  <div itemprop="articleBody">
                    <?php the_excerpt(); ?>

                    <p class="home__meta"><time datetime="<?php the_time('c'); ?>"><?php the_time('d.m.Y'); ?></time></p> 

                    <span class="home__btn">Læs mere</span>  
                  </div>
                </div>

                <div class="home__thumb col-sm-4" style="background-image: url(<?php echo esc_url($thumb[0]); ?>);" >
                  
                </div>

              </div>

            </a>

          <?php endwhile; else: ?>

            <p class="center hpad">Konflikthuset vil løbende blogge om interessante emner.</p>

        <?php endif; ?>

      </div>
    </div>

  </section>

</main>

<?php get_template_part('parts/footer'); ?>