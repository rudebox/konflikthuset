<?php 
/**
* Description: Lionlab contact field group layout
*
* @package Lionlab
* @subpackage Lionlab
* @since Version 1.0
* @author Kaspar Rudbech
*/


//section settings
$bg = get_sub_field('bg');
$title = get_sub_field('header');
$margin = get_sub_field('margin');

?>

<section class="contact <?php echo esc_attr($bg); ?>--bg padding--<?php echo esc_attr($margin); ?>">
	<div class="wrap hpad center">
		<h2 class="contact__header"><?php echo esc_html($title); ?></h2>
		<button id="js-btn-phone" class="btn btn--hollow btn--hollow--green gray contact__btn is-active">Ring mig op</button>
		<button id="js-btn-message" class="btn btn--hollow btn--hollow--green gray contact__btn">Send e-mail</button>
		<div class="row">
			
			<div class="col-sm-10 col-sm-offset-1 contact__item">

				<div id="js-form-phone" class="contact__form is-active">
					<?php gravity_form( 2, $display_title = false, $display_description = false, $display_inactive = false, $field_values = null, $ajax = false, 2, $echo = true ); ?>
				</div>

				<div id="js-form-message" class="contact__form">
					<?php gravity_form( 1, $display_title = false, $display_description = false, $display_inactive = false, $field_values = null, $ajax = false, 2, $echo = true ); ?>
				</div>
			</div>

		</div>
	</div>
</section>