<?php 
	if ( is_tax() ) {
		$term = get_term_by( 'slug', get_query_var( 'term' ), get_query_var( 'taxonomy' ) );
		$title = $term->name;
	} elseif ( is_category() ) {
		$title = single_cat_title( '', false );
	} elseif ( is_archive() ) {
		$title = post_type_archive_title( '', false );
	} else {
		$id = (is_home()) ? get_option('page_for_posts') : $post->ID;
		$title = get_proper_title($id);
	}

	//bg
	$img = get_field('page_img') ? : $img = get_field('page_img', 'options') ;

	//post bg
	$thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'url' );
?>

<?php if (!is_single() ) : ?>
<section class="page__hero" style="background-image: url(<?php echo esc_url($img['url']); ?>);">
<?php else : ?>
<section class="page__hero" style="background-image: url(<?php echo esc_url($thumb[0]); ?>);">
<?php endif; ?>
	<div class="wrap hpad center flex flex--hvalign"> 
		<?php if (!is_single() ) : ?>
		<div class="page__mask" style="background-image: url(<?php echo esc_url($img['url']); ?>);"></div>
		<?php else : ?>
		<div class="page__mask" style="background-image: url(<?php echo esc_url($thumb[0]); ?>);"></div>
		<?php endif; ?>
		<h1 class="page__title"><?php echo esc_html($title); ?></h1>
		<?php
			//breadcrumbs nom nom
			if ( function_exists('yoast_breadcrumb') ) {
				yoast_breadcrumb( '<p class="page__breadcrumbs">','</p>' );
			}
		?>
	</div>
</section>