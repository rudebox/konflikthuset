<?php
/**
 * Description: Lionlab sliders
 *
 * @package Lionlab
 * @subpackage Lionlab
 * @since Version 1.0
 * @author Kaspar Rudbech
*/

if ( have_rows('slides') ) : ?>

  <section class="slider">
    <div class="slider__track is-slider">

      <?php
      // Loop through slides
      while ( have_rows('slides') ) :
        the_row();
        $image   = get_sub_field('slides_bg');
        $title = get_sub_field('slides_title');
        $caption = get_sub_field('slides_text'); ?>

        <div class="slider__item flex flex--valign" style="background-image: url(<?php echo esc_url($image['url']); ?>);">
          <div class="wrap hpad slider__container center">
            <div class="row">

              <div class="col-sm-8 col-sm-offset-2 col-md-6 col-md-offset-3">
                <div class="slider__text">
                  <h1 class="slider__title h1"><?php echo esc_html($title); ?></h1>
                  <?php echo $caption; ?>
                </div>
              </div>

            </div>
          </div>
        </div>

      <?php endwhile; ?>

    </div>
  </section>
<?php endif; ?>