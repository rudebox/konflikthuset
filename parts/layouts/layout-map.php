<?php 
/**
* Description: Lionlab map field group layout
*
* @package Lionlab
* @subpackage Lionlab
* @since Version 1.0
* @author Kaspar Rudbech
*/

//sections settings
$bg = get_sub_field('bg');
$margin = get_sub_field('margin');
$title = get_sub_field('header');

$img = get_sub_field('map_img');
?>

<section class="map <?php echo esc_attr($bg); ?>--bg padding--<?php echo esc_attr($margin); ?>">
	<div class="wrap hpad">
		<h2 class="cta__header center"><?php echo esc_html($title); ?></h2>
		<div class="row">

			<div class="col-sm-10 col-sm-offset-1 map__item center">
				<?php echo file_get_contents($img['url']); ?>
			</div>

		</div>
	</div>
</section>