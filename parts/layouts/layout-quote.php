<?php 
/**
* Description: Lionlab quote field group layout
*
* @package Lionlab
* @subpackage Lionlab
* @since Version 1.0
* @author Kaspar Rudbech
*/

//sections settings
$quote = get_sub_field('quote_text');

?>

<section class="quote green--bg">
	<div class="wrap hpad">
		<div class="row">

			<div class="col-sm-10 col-sm-offset-1 quote__item center">
				<i class="fas fa-quote-right"></i>
				<h2 class="quote__text"><?php echo esc_html($quote); ?></h2>
			</div>

		</div>
	</div>
</section>