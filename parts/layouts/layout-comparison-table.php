<?php 
/**
* Description: Lionlab comparison-table repeater field group layout
*
* @package Lionlab
* @subpackage Lionlab
* @since Version 1.0
* @author Kaspar Rudbech
*/

//sections settings
$bg = get_sub_field('bg');
$margin = get_sub_field('margin');
$title = get_sub_field('header');

if (have_rows('tables') ) :
?>

<section class="comparison-table <?php echo esc_attr($bg); ?>--bg padding--<?php echo esc_attr($margin); ?>">
	<div class="wrap hpad">
		<h2 class="comparison-table__header center"><?php echo esc_html($title); ?></h2>
		<div class="row flex flex--wrap">

			<?php while (have_rows('tables') ) : the_row(); 
				$title = get_sub_field('table_title');
				$text = get_sub_field('table_text');
			?>

			<div class="col-sm-6 comparison-table__item center">
				<h3 class="comparison-table__title"><?php echo esc_html($title); ?></h3>
				<?php echo $text; ?>
			</div>
			<?php endwhile; ?>

		</div>
	</div>
</section>
<?php endif; ?>