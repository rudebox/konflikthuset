<?php 
/**
* Description: Lionlab downloads repeater field group layout
*
* @package Lionlab
* @subpackage Lionlab
* @since Version 1.0
* @author Kaspar Rudbech
*/

//sections settings
$bg = get_sub_field('bg');
$margin = get_sub_field('margin');
$title = get_sub_field('header');
$center = get_sub_field('center');

if ($center === true) {
	$center = 'center';
}

if (have_rows('downloads') ) :
?>

	<section class="download <?php echo esc_attr($bg); ?>--bg padding--<?php echo esc_attr($margin); ?>">
		<div class="wrap hpad">

			<?php if ($title) : ?>
			<h2 class="download__header <?php echo esc_attr($center); ?>"><?php echo esc_html($title); ?></h2>
			<?php endif; ?>

			<div class="row">
				
				<?php 
					while (have_rows('downloads') ) : the_row(); 
						$file = get_sub_field('download_file');
						$file_name = get_sub_field('download_file_name');
				?>

					<a download href="<?php echo esc_url($file['url']); ?>" class="col-sm-8 col-sm-offset-2 download__item flex flex--justify flex--center">
						<span><?php echo esc_html($file_name); ?></span> <i class="fas fa-download"></i>
					</a>

				<?php endwhile; ?>
				
			</div>
		</div>
	</section>

<?php endif; ?>