<?php 
/**
* Description: Lionlab link-boxes repeater field group layout
*
* @package Lionlab
* @subpackage Lionlab
* @since Version 1.0
* @author Kaspar Rudbech
*/

//sections settings
$bg = get_sub_field('bg');
$margin = get_sub_field('margin');
$title = get_sub_field('header');

if (have_rows('linkbox') ) :
?>

<section class="link-boxes <?php echo esc_attr($bg); ?>--bg padding--<?php echo esc_attr($margin); ?>">
	<div class="wrap hpad">
		<h2 class="link-boxes__header center"><?php echo esc_html($title); ?></h2>
		<div class="row flex flex--wrap">
			<?php while (have_rows('linkbox') ) : the_row(); 
				$title = get_sub_field('title');
				$text = get_sub_field('text');
				$icon = get_sub_field('icon');
				$icon_copy = get_sub_field('icon_copy');
			?>

			<div class="col-sm-3 link-boxes__item">
				<div class="link-boxes__wrapper">
					<?php if ($icon) : ?>
					<picture>
						<source srcset="<?php echo esc_url($icon_copy['sizes']['offers']); ?>" type="image/webp">
						<source srcset="<?php echo esc_url($icon['sizes']['offers']); ?>" type="image/jpeg"> 
						<img class="link-boxes__img" src="<?php echo esc_url($icon['sizes']['offers']); ?>" alt="<?php echo esc_attr($icon['alt']); ?>">
					</picture>
					<?php endif; ?>

					<h3 class="link-boxes__title"><?php echo $title; ?></h3>
				</div>
				<div class="link-boxes__content">

					<?php if (have_rows('links') ) : while (have_rows('links') ) : the_row(); 
						$link = get_sub_field('link', false, false);
						
					?>

					<a class="link-boxes__links" href="<?php echo get_permalink($link); ?>"><span>- </span><?php echo get_the_title($link);  ?></a>
					<?php endwhile;  endif; ?>

				</div>

			</div>
			<?php endwhile; ?>
		</div>
	</div>
</section>
<?php endif; ?>