<?php 
/**
* Description: Lionlab cta field group layout
*
* @package Lionlab
* @subpackage Lionlab
* @since Version 1.0
* @author Kaspar Rudbech
*/

//sections settings
$bg = get_sub_field('bg');
$margin = get_sub_field('margin');
$title = get_sub_field('header');

$text = get_sub_field('cta_text');
$img = get_sub_field('cta_bg');
?>

<section class="cta <?php echo esc_attr($bg); ?>--bg padding--<?php echo esc_attr($margin); ?>" style="background-image: url(<?php echo $img['url']; ?>)">
	<div class="wrap hpad">
		<h2 class="cta__header center"><?php echo esc_html($title); ?></h2>
		<div class="row ">

			<div class="col-sm-10 col-sm-offset-1 cta__item center">
				<?php echo $text; ?>
			</div>

		</div>
	</div>
</section>