<?php
if( have_rows('layout') ) {
  $GLOBALS['layout_count'] = 1; while ( have_rows('layout') ) { the_row();
    if( get_row_layout() === 'hero_unit' ) { ?>

      <?php get_template_part( 'parts/layouts/layout', 'hero_unit' ); ?>

    <?php
    } elseif( get_row_layout() === 'slider' ) {
    ?>

      <?php get_template_part( 'parts/layouts/layout', 'slider' ); ?>

    <?php
    } elseif( get_row_layout() === 'download' ) {
    ?>

      <?php get_template_part( 'parts/layouts/layout', 'download' ); ?>

    <?php
    } elseif( get_row_layout() === 'cta' ) {
    ?>

      <?php get_template_part( 'parts/layouts/layout', 'cta' ); ?>

    <?php
    } elseif( get_row_layout() === 'map' ) {
    ?>

      <?php get_template_part( 'parts/layouts/layout', 'map' ); ?>

    <?php
    } elseif( get_row_layout() === 'contact' ) {
    ?>

      <?php get_template_part( 'parts/layouts/layout', 'contact' ); ?>

    <?php
    } elseif( get_row_layout() === 'link-boxes' ) {
    ?>

      <?php get_template_part( 'parts/layouts/layout', 'link-boxes' ); ?>

    <?php
    } elseif( get_row_layout() === 'comparison-table' ) {
    ?>

      <?php get_template_part( 'parts/layouts/layout', 'comparison-table' ); ?>

    <?php
    } elseif( get_row_layout() === 'quote' ) {
    ?>

      <?php get_template_part( 'parts/layouts/layout', 'quote' ); ?>

    <?php
    } elseif( get_row_layout() === 'wysiwygs' ) {
    ?>

      <?php get_template_part( 'parts/layouts/layout', 'wysiwygs' ); ?>

    <?php
    }
    ?>

  <?php
    $GLOBALS['layout_count']++;
  }
  ?>

<?php
} else {
?>
  <p class="center">You haven't added any layouts yet. <?php edit_post_link('Add one now.'); ?></p>
<?php
}
?>
