<?php

/*
 * Template Name: Recommend a friend template
 */

get_template_part('parts/header'); the_post(); 

$title = get_field('recommend_title');
$form_id = get_field('recommend_form_id');
$text = get_field('recommend_text');


?>

<main>

	<?php get_template_part('parts/page', 'header');?>

	<section class="contact padding--both">
		<div class="wrap hpad">
			<div class="row">

				<div class="col-sm-6 contact__info">
					<h2><?php echo esc_html($title); ?></h2>
					<?php echo $text; ?>
				</div>

				<div class="col-sm-6">
					<?php 
						gravity_form( $form_id, $display_title = false, $display_description = false, $display_inactive = false, $field_values = null, $ajax = false, 2, $echo = true ); 
					?>
				</div> 

			</div>
		</div>
	</section>
</main>

<?php get_template_part('parts/footer'); ?>